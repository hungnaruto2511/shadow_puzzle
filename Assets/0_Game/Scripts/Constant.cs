public class Constant
{
    // anim
    public const string ANIM_IDLE = "idle";
    public const string ANIM_RUN = "run";

    // scene
    public const string SCENE_LOAIDING = "Loading";
    public const string SCENE_HOME = "Home";

}
