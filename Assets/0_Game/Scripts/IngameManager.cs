using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;


public class IngameManager : MonoBehaviour
{
    public static IngameManager ins;
/*    public Camera mainCamera;
*//*    public Image splashPanel;
*/  /*  public GameObject obj_denSanKhau;*/
/*    public Image panelMaskBG;
*/    public LevelManager levelManager;

    public Material[] char_mat;
    public GameObject obj_tutRotate;
    public GameObject obj_tutSwipe;
    public int soLuongLevel = 1;
    public Image img_process;
    public TMP_Text txt_process;
    public GameObject obj_hint;

    private void Awake()
    {
        ins = this;
    }

    private IEnumerator Start()
    {
        if(GameManager.ins == null)
        {
            SceneManager.LoadScene(Constant.SCENE_LOAIDING);
        }


        levelManager = Instantiate(Resources.Load<LevelManager>("Level/level_" + (DataManager.ins.playerData.level%soLuongLevel+1)));
/*
        obj_denSanKhau.SetActive(false);
        levelManager.objToRotate.SetActive(false);*/

        SoundManager.PlayEfxSound(SoundManager.ins.moDauLevel);
/*        splashPanel.DOFade(0, 3f);
*/
        yield return Cache.GetWFS(5);

/*        obj_denSanKhau.SetActive(true);
*/       /* levelManager.objToRotate.SetActive(true);*/
    }
}
