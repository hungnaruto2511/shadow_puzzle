using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CanvasInGame : MonoBehaviour
{
    [SerializeField] private TMP_Text levelText;
    public void OnEnable()
    {
        UpdateUI();
    }
    private void UpdateUI() {
        var playerData = DataManager.ins.playerData;
        levelText.text = "level " + (playerData.level + 1).ToString();
    }
}
