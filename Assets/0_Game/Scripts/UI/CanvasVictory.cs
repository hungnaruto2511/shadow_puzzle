using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasVictory : UICanvas
{
    public override void Open()
    {
        base.Open();
    }
    
    public override void Close()
    {
        base.Close();
    }

    public void Btn_nextLevel()
    {
        DataManager.ins.playerData.level++;
        SceneManager.LoadScene(Constant.SCENE_HOME);
        Close();
    }
}
