using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISettingController : MonoBehaviour
{
    [SerializeField] private UiSettingGroupController soundGroup;
    [SerializeField] private UiSettingGroupController musicGroup;

    private void OnEnable()
    {
        Time.timeScale = 0.0f;
        UpdateUI();
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
    }
    public void UpdateUI()
    {
        var playerData= DataManager.ins.playerData;
        soundGroup.UpdateState(playerData.isSound);
        musicGroup.UpdateState(playerData.isMusic);
    }

    /// <summary>
    /// be called from sound button
    /// </summary>
    public void SoundTrigger() 
    {
        var playerData = DataManager.ins.playerData;
        playerData.isSound = !playerData.isSound;
        UpdateUI();
    }
    /// <summary>
    /// be called from sound button
    /// </summary>
    public void MusicTriggers()
    {
        var playerData = DataManager.ins.playerData;
        playerData.isMusic = !playerData.isMusic;
        UpdateUI();
    }

    public void ButtonClose()
    {
        gameObject.SetActive(false);
    }
}
