using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiSettingGroupController : MonoBehaviour
{
    [SerializeField] private GameObject on;
    [SerializeField] private GameObject off;
    public void UpdateState(bool isOn)
    {
        on.SetActive(isOn);
        off.SetActive(!isOn);
    }
}
