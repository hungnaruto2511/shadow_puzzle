using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPiece : MonoBehaviour
{
    public float randomZ = 2f;
    public CopyRotate copyRotate;
    public Transform[] pieces;

    private void Start()
    {
        if(DataManager.ins.playerData.level == 0)
        {
            // tutorial
            for (int i = 0; i < pieces.Length; i++)
            {
                float rand = Random.Range(-randomZ, randomZ);
                pieces[i].position += transform.forward * rand;
            }
            
            transform.eulerAngles = new Vector3(0, Random.Range(10, 170), 0);
        }
        else
        {
            for (int i = 0; i < pieces.Length; i++)
            {
                float rand = Random.Range(-2f, 2f);
                pieces[i].position += transform.forward * rand;
            }
            
            transform.eulerAngles = new Vector3(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));
        }
            
    }
}
