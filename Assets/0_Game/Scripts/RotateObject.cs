using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateObject : MonoBehaviour
{
    // public CopyRotate copyRotate;
    public float rotateSpeed = 4;
    private bool isDragging = false;
    public Rigidbody _rigidbody;
    private float oldX;
    private float oldY;
    private bool done = false;
    public int indexStep = 0;
    private bool waitStep = false;

    private void Start()
    {
        if(DataManager.ins.playerData.level == 0 || DataManager.ins.playerData.level == 1)
        {
            IngameManager.ins.obj_tutRotate.SetActive(true);
        }

        IngameManager.ins.obj_hint.SetActive(true);
        IngameManager.ins.img_process.fillAmount = 0;
    }


    private void Update()
    {
        if(waitStep) return;
        // copyRotate._transform.rotation = transform.rotation;

        if(done) return;
        if(Input.GetMouseButtonDown(0))
        {
            isDragging = true;
            oldX = Input.mousePosition.x;
            oldY = Input.mousePosition.y;

            IngameManager.ins.obj_tutRotate.SetActive(false);
            IngameManager.ins.obj_tutSwipe.SetActive(false);
        }
        else if(Input.GetMouseButtonUp(0))
        {
            isDragging = false;

            if(indexStep == 1)
            {
                IngameManager.ins.obj_tutSwipe.SetActive(true);
            }
        }


        if(isDragging)
        {

            if(DataManager.ins.playerData.level == 0)
            {
                float currentX = Input.mousePosition.x;
                float mouseMoveX = currentX - oldX;

                _rigidbody.AddTorque(Vector3.down * mouseMoveX * rotateSpeed);

                oldX = Input.mousePosition.x;
            }   
            else
            {
                if(indexStep == 0)
                {
                    float currentX = Input.mousePosition.x;
                    float mouseMoveX = currentX - oldX;
                    float currentY = Input.mousePosition.y;
                    float mouseMoveY = currentY - oldY;

                    _rigidbody.AddTorque(Vector3.down * mouseMoveX * rotateSpeed);
                    _rigidbody.AddTorque(Vector3.right * mouseMoveY * rotateSpeed);

                    oldX = Input.mousePosition.x;
                    oldY = Input.mousePosition.y;
                }
                else
                {
                    float currentY = Input.mousePosition.y;
                    float mouseMoveY = currentY - oldY;

                    _rigidbody.AddTorque(Vector3.forward * mouseMoveY * rotateSpeed * 0.7f);

                    oldY = Input.mousePosition.y;
                }
            }

        }


        if(indexStep == 0)
        {
            // if(DataManager.ins.playerData.level == 0)
            // {
                IngameManager.ins.img_process.fillAmount = Mathf.Max(1 - Vector3.Angle(transform.forward, Vector3.forward) / 90, 
                1 - Vector3.Angle(transform.forward, -Vector3.forward) / 90f);
            // }
            // else
            // {
            //     IngameManager.ins.img_process.fillAmount = Mathf.Max(1 - Vector3.Angle(transform.forward, Vector3.forward) / 90, 
            //     1 - Vector3.Angle(transform.forward, -Vector3.forward) / 90f) * 0.9f;
            // }

            if(Vector3.Angle(transform.forward, Vector3.forward) < 2)
            {
                // done = true;
                // Debug.LogError("done");
                // _rigidbody.isKinematic = true;
                waitStep = true;

                IngameManager.ins.img_process.DOFillAmount(1, 1);
                transform.DORotate(new Vector3(0,0,transform.eulerAngles.z), 1f, RotateMode.Fast).OnComplete(() => {
                    // LevelManager.ins.MoveToCorrect();
                    waitStep = false;
                    indexStep = 1;
                    isDragging = false;
                    IngameManager.ins.obj_hint.gameObject.SetActive(false);

                    if(DataManager.ins.playerData.level > 0)
                    {
                        IngameManager.ins.obj_tutSwipe.SetActive(true);
                    }
                });
            }
            else if(Vector3.Angle(transform.forward, -Vector3.forward) < 2)
            {
                // done = true;
                // Debug.LogError("done");
                // _rigidbody.isKinematic = true;
                waitStep = true;

                IngameManager.ins.img_process.DOFillAmount(1, 1);
                transform.DORotate(new Vector3(0,180,transform.eulerAngles.z), 1f, RotateMode.Fast).OnComplete(() => {
                    // LevelManager.ins.MoveToCorrect();
                    waitStep = false;
                    indexStep = 1;
                    isDragging = false;
                    IngameManager.ins.obj_hint.gameObject.SetActive(false);

                    if(DataManager.ins.playerData.level > 0)
                    {
                        IngameManager.ins.obj_tutSwipe.SetActive(true);
                    }
                });
            }
        }
        else
        {
            if(Vector3.Angle(transform.up, Vector3.up) < 2)
            {
                done = true;
                Debug.LogError("done");
                // _rigidbody.isKinematic = true;
                waitStep = true;

                if(LevelManager.ins.indexScene == 0)
                {
                    LevelManager.ins.correctObj.SetActive(true);
                    LevelManager.ins.objToRotate.SetActive(false);
                    LevelManager.ins.correctObj.transform.position = LevelManager.ins.objToRotate.transform.position;
                    if(LevelManager.ins.levelLoi == false)
                    {
                        
                        LevelManager.ins.correctObj.transform.rotation = LevelManager.ins.objToRotate.transform.rotation;
                    }
                    else
                    {
                        LevelManager.ins.correctObj.transform.eulerAngles = new Vector3(LevelManager.ins.objToRotate.transform.eulerAngles.x, 
                        LevelManager.ins.objToRotate.transform.eulerAngles.y + 90,
                        LevelManager.ins.objToRotate.transform.eulerAngles.z);
                    }
                    LevelManager.ins.correctObj.transform.localScale = LevelManager.ins.objToRotate.transform.localScale;
                    IngameManager.ins.obj_hint.SetActive(false);

                    // LevelManager.ins.correctObj.transform.DOMove(correctPos.position, 1);
                    // LevelManager.ins.correctObj.transform.DORotateQuaternion(correctPos.rotation, 1);
                    // LevelManager.ins.correctObj.transform.DOScale(correctPos.localScale, 1);
                }
                else
                {
                    LevelManager.ins.correctObj2.SetActive(true);
                    LevelManager.ins.objToRotate2.SetActive(false);
                    LevelManager.ins.correctObj2.transform.position = LevelManager.ins.objToRotate2.transform.position;

                    if(LevelManager.ins.levelLoi == false)
                    {
                        
                        LevelManager.ins.correctObj2.transform.rotation = LevelManager.ins.objToRotate2.transform.rotation;
                    }
                    else
                    {
                        LevelManager.ins.correctObj2.transform.eulerAngles = new Vector3(LevelManager.ins.objToRotate2.transform.eulerAngles.x, 
                        LevelManager.ins.objToRotate2.transform.eulerAngles.y + 180,
                        LevelManager.ins.objToRotate2.transform.eulerAngles.z);
                    }
                    
                    // LevelManager.ins.correctObj2.transform.rotation = LevelManager.ins.objToRotate2.transform.rotation;
                    LevelManager.ins.correctObj2.transform.localScale = LevelManager.ins.objToRotate2.transform.localScale;

                    // correctObj2.transform.DOMove(correctPos2.position, 1);
                    // correctObj2.transform.DORotateQuaternion(correctPos2.rotation, 1);
                    // correctObj2.transform.DOScale(correctPos2.localScale, 1);
                }

                IngameManager.ins.obj_tutSwipe.SetActive(false);
                transform.DORotate(new Vector3(0,0,transform.eulerAngles.z), 1f, RotateMode.Fast).OnComplete(() => {
                    LevelManager.ins.MoveToCorrect();
                    // waitStep = false;
                    indexStep = 2;
                });
            }
        }
    }
}
