using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DenSanKhau : MonoBehaviour
{
    private void OnEnable()
    {
        SoundManager.PlayEfxSound(SoundManager.ins.batDen);
    }

    private void OnDisable()
    {
        SoundManager.PlayEfxSound(SoundManager.ins.batDen);
    }
}
