using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [SerializeField] private List<Rigidbody> rbs;
    private bool isFalling = false;
    private void Awake()
    {
        rbs= GetComponentsInChildren<Rigidbody>().ToList();
        foreach(Rigidbody rb in rbs)
        {
            rb.isKinematic = true;
        }
    }
    [ContextMenu("Fall")]
    public void Fall()
    {
        isFalling = true;
        foreach (Rigidbody rb in rbs)
        {
            rb.isKinematic = false;
           
        }
        GetComponent<Animator>().enabled = false;
    }

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (isFalling) return;
        if(collision.transform.TryGetComponent<ObsFalling>(out var obsFall))
        {
            Fall();
        }
    }


    public void Cry()
    {

    }
}
