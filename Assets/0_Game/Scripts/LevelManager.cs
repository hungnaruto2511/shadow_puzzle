using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.Feedbacks;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static LevelManager ins;
    public Animator playerAnimator;
    // public Transform shadowObj;
    public Transform camera_trans;

    [Header("-------- objs canh 1 -----------")]
    public Transform correctPos;
    public GameObject correctObj;
    public GameObject objToRotate;

    [Header("-------- objs canh 2 -----------")]
    public Transform correctPos2;
    public GameObject correctObj2;
    public GameObject objToRotate2;

    [Header("------------------------")]
    public Material[] level_mat;
    [SerializeField] private MMF_Player initFeedBack;
    [SerializeField] private MMF_Player secondFeedBack;
    [SerializeField] private MMF_Player winFeedBack;
    public Image img_bg;
    public Image img_target;
    public Sprite bg_sprite;
    public Sprite sprite_target;
    public Transform fx_heart;
    public Transform male;
    public Transform female;
    public float timeSceneWin = 5;
    public int indexScene = 0;
    public int totalScene = 1;
    public bool levelLoi = false;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        img_bg.sprite = bg_sprite;

        if(sprite_target != null) img_target.sprite = sprite_target;

        correctObj.SetActive(false);
        // level_mat.SetFloat("_Float", 0);
        initFeedBack.PlayFeedbacks();

        for (int i = 0; i < level_mat.Length; i++)
        {
            level_mat[i].SetFloat("_Float", 0);
        }
        for (int i = 0; i < IngameManager.ins.char_mat.Length; i++)
        {
            IngameManager.ins.char_mat[i].SetFloat("_Float", 0);
        }
    }

    private void Update()
    {
        for (int i = 0; i < level_mat.Length; i++)
        {
            level_mat[i].SetVector("_LightSource", camera_trans.position);
        }
        for (int i = 0; i < IngameManager.ins.char_mat.Length; i++)
        {
            IngameManager.ins.char_mat[i].SetVector("_LightSource", camera_trans.position);
        }
    }

    public void MoveToCorrect()
    {
        StartCoroutine(ie_MoveToCorrect());
    }

    private IEnumerator ie_MoveToCorrect()
    {
/*        yield return Cache.GetWFS(1f);
*/
        if(indexScene == 0)
        {
            // correctObj.SetActive(true);
            // objToRotate.SetActive(false);
            // correctObj.transform.position = objToRotate.transform.position;
            // correctObj.transform.rotation = objToRotate.transform.rotation;
            // correctObj.transform.localScale = objToRotate.transform.localScale;

            correctObj.transform.DOMove(correctPos.position, 1);
            // if(levelLoi == false)
            // {
                correctObj.transform.DORotateQuaternion(correctPos.rotation, 1);
            // }
            // else
            // {
            //     correctObj.transform.DORotate(new Vector3(correctPos.rotation.eulerAngles.x, correctPos.rotation.eulerAngles.y-90, correctPos.rotation.eulerAngles.z), 1);
            // }
            correctObj.transform.DOScale(correctPos.localScale, 1);
        }
        else
        {
            // correctObj2.SetActive(true);
            // objToRotate2.SetActive(false);
            // correctObj2.transform.position = objToRotate2.transform.position;
            // correctObj2.transform.rotation = objToRotate2.transform.rotation;
            // correctObj2.transform.localScale = objToRotate2.transform.localScale;

            correctObj2.transform.DOMove(correctPos2.position, 1);
            correctObj2.transform.DORotateQuaternion(correctPos2.rotation, 1);
            correctObj2.transform.DOScale(correctPos2.localScale, 1);
        }

        yield return Cache.GetWFS(1);

        indexScene++;

        // đây là win
        if(indexScene == totalScene)
        {
            yield return Cache.GetWFS(1);

            // winFeedBack.PlayFeedbacks();

            float angle = 0;
            DOTween.To(() => angle, x => angle = x, 1f, 2)
                .OnUpdate(() => {
                    for (int i = 0; i < level_mat.Length; i++)
                    {
                        level_mat[i].SetFloat("_Float", angle);
                    }
                    for (int i = 0; i < IngameManager.ins.char_mat.Length; i++)
                    {
                        IngameManager.ins.char_mat[i].SetFloat("_Float", angle);
                    }
                });

            var playerData=DataManager.ins.playerData;
            if(playerData.level%5==3){
                winFeedBack.PlayFeedbacks();
            }
            else
            {

                yield return Cache.GetWFS(1.5f);
                var mainCam = Camera.main;
                mainCam.transform.DORotate(new Vector3(15,0,0), 2);
                mainCam.transform.DOMoveY(mainCam.transform.position.y + 2, 2).OnComplete(() =>
                {
                    winFeedBack.PlayFeedbacks();
                });
            }

            yield return Cache.GetWFS(timeSceneWin);

            // var mainCam = Camera.main;
            // mainCam.transform.DORotate(new Vector3(15,0,0), 2);
            // mainCam.transform.DOMoveY(mainCam.transform.position.y + 2, 2);

            fx_heart.gameObject.SetActive(true);
            fx_heart.transform.position = (male.position + female.position) * 0.5f + Vector3.up;

            
            
            // yield return Cache.GetWFS(2);

            // float angle = 0;
            // DOTween.To(() => angle, x => angle = x, 1f, 2)
            //     .OnUpdate(() => {
            //         for (int i = 0; i < level_mat.Length; i++)
            //         {
            //             level_mat[i].SetFloat("_Float", angle);
            //         }
            //         for (int i = 0; i < IngameManager.ins.char_mat.Length; i++)
            //         {
            //             IngameManager.ins.char_mat[i].SetFloat("_Float", angle);
            //         }
            //     });

            yield return Cache.GetWFS(2);

            
            UIManager.ins.OpenUI(UIID.UICVictory);
        }
        else
        {
            // bắt đầu cảnh 2
            secondFeedBack.PlayFeedbacks();

        }
    }


}
